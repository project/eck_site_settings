<?php

namespace Drupal\eck_site_settings_domain;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\eck\EckEntityBundleInterface;
use Drupal\eck_site_settings\SettingsRepositoryInterface;

/**
 * Alters the ECK bundle form.
 */
class EckBundleFormAlter {

  use StringTranslationTrait;

  /**
   * The settings repository.
   *
   * @var \Drupal\eck_site_settings\SettingsRepositoryInterface
   */
  protected $settings;

  /**
   * Constructs a new EckEntityTypeFormEventSubscriber object.
   *
   * @param \Drupal\eck_site_settings\SettingsRepositoryInterface $settings
   *   The settings repository.
   */
  public function __construct(
    SettingsRepositoryInterface $settings
  ) {
    $this->settings = $settings;
  }

  /**
   * Add a setting to ECK bundle forms.
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    $bundle = $form_state->getFormObject()->getEntity();
    assert($bundle instanceof EckEntityBundleInterface);

    if (!isset($form['settings'])) {
      $form['settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Settings'),
      ];
    }

    $form['settings']['eck_site_settings_domain'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow different settings per domain'),
      '#default_value' => $bundle->getThirdPartySetting('eck_site_settings_domain', 'enabled', FALSE),
    ];

    $form['#entity_builders'][] = [static::class, 'formBuilder'];
  }

  /**
   * Store the setting in the entity type's third party settings.
   */
  public static function formBuilder(string $entityTypeId, EckEntityBundleInterface $bundle, array &$form, FormStateInterface $formState): void {
    $enabled = $formState->getValue('eck_site_settings_domain');
    $bundle->setThirdPartySetting('eck_site_settings_domain', 'enabled', $enabled);
  }

}
