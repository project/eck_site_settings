<?php

namespace Drupal\eck_site_settings_domain;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Installs the domain access field.
 */
class FieldInstaller {

  protected const FIELD_NAME = 'field_domain_access';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * FieldInstaller constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Installs the domain access field.
   */
  public function installField(string $entityTypeId, string $bundle): void {
    $storage = $this->entityTypeManager->getStorage('field_config');

    $id = $entityTypeId . '.' . $bundle . '.' . static::FIELD_NAME;
    if ($storage->load($id)) {
      return;
    }

    $values = [
      'field_name' => static::FIELD_NAME,
      'entity_type' => $entityTypeId,
      'label' => 'Domain Access',
      'bundle' => $bundle,
      'required' => TRUE,
      'settings' => [
        'handler' => 'default:domain',
        'target_bundles' => NULL,
        'sort' => ['field' => 'weight', 'direction' => 'ASC'],
      ],
    ];

    $field = $storage->create($values);
    $field->save();
  }

  /**
   * Installs the domain access field storage.
   */
  public function installFieldStorage(string $entityTypeId): void {
    $storage = $this->entityTypeManager->getStorage('field_storage_config');

    $id = $entityTypeId . '.' . static::FIELD_NAME;
    if ($storage->load($id)) {
      return;
    }

    $values = [
      'field_name' => static::FIELD_NAME,
      'entity_type' => $entityTypeId,
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'domain',
      ],
      'cardinality' => 1,
    ];

    $field = $storage->create($values);
    $field->save();
  }

}
