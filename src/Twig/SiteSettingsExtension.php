<?php

namespace Drupal\eck_site_settings\Twig;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Render\RendererInterface;
use Drupal\eck\EckEntityInterface;
use Drupal\eck_site_settings\SettingsRepositoryInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension for site settings.
 */
class SiteSettingsExtension extends AbstractExtension {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The settings service.
   *
   * @var \Drupal\eck_site_settings\SettingsRepositoryInterface
   */
  protected $settings;

  /**
   * SettingsExtension constructor.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\eck_site_settings\SettingsRepositoryInterface $settings
   *   The settings repository.
   */
  public function __construct(
    RendererInterface $renderer,
    SettingsRepositoryInterface $settings
  ) {
    $this->renderer = $renderer;
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions(): array {
    return [
      new TwigFunction('site_settings', [$this, 'getSetting']),
    ];
  }

  /**
   * Get a site setting entity.
   */
  public function getSetting(?string $bundle, ?string $entityTypeId = 'settings', array $context = []): ?EckEntityInterface {
    if (empty($bundle)) {
      return NULL;
    }

    $entity = $this->settings->getSetting($bundle, $entityTypeId, $context);

    // Workaround to include caching metadata of the settings entity.
    $build = [];
    CacheableMetadata::createFromObject($entity)
      ->applyTo($build);
    $this->renderer->render($build);

    return $entity;
  }

}
