<?php

namespace Drupal\eck_site_settings;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\eck\EckEntityBundleInterface;
use Drupal\eck\EckEntityInterface;
use Drupal\eck\EckEntityTypeInterface;

/**
 * A service for loading settings related entity types, bundles and entities.
 */
class SettingsRepository implements SettingsRepositoryInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The entity type repository.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepositoryInterface
   */
  protected EntityTypeRepositoryInterface $entityTypeRepository;

  /**
   * The static cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $staticCache;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * SettingsRepository constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityTypeRepositoryInterface $entityTypeRepository
   *   The entity type repository.
   * @param \Drupal\Core\Cache\CacheBackendInterface $staticCache
   *   The static cache.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityTypeRepositoryInterface $entityTypeRepository,
    CacheBackendInterface $staticCache,
    LanguageManagerInterface $languageManager,
    ModuleHandlerInterface $moduleHandler
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityTypeRepository = $entityTypeRepository;
    $this->staticCache = $staticCache;
    $this->languageManager = $languageManager;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypes(): array {
    $storage = $this->entityTypeManager
      ->getStorage('eck_entity_type');
    $ids = $storage->getQuery()
      ->condition('third_party_settings.eck_site_settings.enabled', TRUE)
      ->sort('label')
      ->accessCheck(FALSE)
      ->execute();

    if ($ids === []) {
      return [];
    }

    return $storage->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getBundles(EckEntityTypeInterface $entityType): array {
    if (!$this->entityTypeManager->hasDefinition($entityType->id())) {
      return [];
    }

    $definition = $this->entityTypeManager
      ->getDefinition($entityType->id());
    $storage = $this->entityTypeManager
      ->getStorage($definition->getBundleEntityType());

    return $storage->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function isSetting(string $entityTypeId): bool {
    $cid = "eck_site_settings:is_setting:{$entityTypeId}";
    if ($cache = $this->staticCache->get($cid)) {
      return (bool) $cache->data;
    }

    $storage = $this->entityTypeManager
      ->getStorage('eck_entity_type');
    $query = $storage->getQuery()
      ->condition('id', $entityTypeId)
      ->condition('third_party_settings.eck_site_settings.enabled', TRUE)
      ->accessCheck(FALSE);

    $result = (bool) $query->count()
      ->execute();
    $this->staticCache->set($cid, $result, Cache::PERMANENT, ["eck_entity_type:{$entityTypeId}"]);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting(string $bundle, string $entityTypeId = 'settings', array $context = []): EckEntityInterface {
    $context['langcode'] ??= $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    $context['entityTypeId'] = $entityTypeId;
    $context['bundle'] = $bundle;
    $this->moduleHandler->alter('eck_site_setting_context', $context);

    $cid = "eck_site_settings:setting_id";
    foreach (array_diff_key($context, ['langcode' => NULL]) as $key => $value) {
      $cid .= ":{$key}={$value}";
    }

    $storage = $this->entityTypeManager
      ->getStorage($entityTypeId);

    if ($cache = $this->staticCache->get($cid)) {
      $entity = $storage->load($cache->data);

      if ($entity instanceof EckEntityInterface) {
        if ($entity->hasTranslation($context['langcode'])) {
          return $entity->getTranslation($context['langcode']);
        }

        return $entity;
      }
    }

    $query = $storage->getQuery()
      ->range(0, 1)
      ->accessCheck(FALSE);

    $values = $this->getValues($bundle, $entityTypeId, $context);
    foreach (array_diff_key($values, ['langcode' => NULL]) as $fieldName => $value) {
      $query->condition($fieldName, $value);
    }

    $ids = $query->execute();
    if ($ids === []) {
      $entity = $this->createEntity($bundle, $entityTypeId, $values);
    }
    else {
      $entity = $storage->load(reset($ids));
    }

    $this->staticCache->set($cid, $entity->id(), Cache::PERMANENT, $entity->getCacheTags());

    if ($entity->hasTranslation($values['langcode'])) {
      return $entity->getTranslation($values['langcode']);
    }

    $translation = $entity->addTranslation($values['langcode']);
    $translation->save();

    return $translation;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettingByClass(string $className, array $context = []): ?EckEntityInterface {
    $context['className'] = $className;
    $this->moduleHandler->alter('eck_site_setting_context', $context);

    $cid = "eck_site_settings:setting_class";
    foreach (array_diff_key($context, ['langcode' => NULL]) as $key => $value) {
      $cid .= ":{$key}={$value}";
    }

    if ($cache = $this->staticCache->get($cid)) {
      [$entityTypeId, $id] = $cache->data;
      $entity = $this->entityTypeManager
        ->getStorage($entityTypeId)
        ->load($id);

      if ($entity instanceof EckEntityInterface) {
        if ($entity->hasTranslation($context['langcode'])) {
          return $entity->getTranslation($context['langcode']);
        }

        return $entity;
      }
    }

    $entityTypeId = $this->entityTypeRepository
      ->getEntityTypeFromClass($className);
    $storage = $this->entityTypeManager
      ->getStorage($entityTypeId);

    if (!method_exists($storage, 'getBundleFromClass')) {
      throw new \LogicException('Entity bundle classes were introduced in Drupal 9.3.0-alpha1. Please update to be able to use this method.');
    }

    $bundle = $storage->getBundleFromClass($className);
    if ($bundle === NULL) {
      return NULL;
    }

    $entity = $this->getSetting($bundle, $entityTypeId, $context);
    $this->staticCache->set($cid, [$entityTypeId, $entity->id()], Cache::PERMANENT, $entity->getCacheTags());

    return $entity;
  }

  /**
   * Creates a new settings entity.
   *
   * @param string $bundle
   *   The bundle.
   * @param string $entityTypeId
   *   The entity type ID.
   * @param array $values
   *   The values.
   *
   * @return \Drupal\eck\EckEntityInterface
   *   The settings entity.
   *
   * @throws \InvalidArgumentException
   *   Thrown when the bundle does not exist.
   */
  protected function createEntity(string $bundle, string $entityTypeId, array $values): EckEntityInterface {
    $definition = $this->entityTypeManager
      ->getDefinition($entityTypeId);
    $storage = $this->entityTypeManager
      ->getStorage($entityTypeId);
    $bundleEntity = $this->entityTypeManager
      ->getStorage($definition->getBundleEntityType())
      ->load($bundle);

    if (!$bundleEntity instanceof EckEntityBundleInterface) {
      throw new \InvalidArgumentException(sprintf('The bundle "%s" does not exist.', $bundle));
    }

    $values['title'] = $bundleEntity->label();
    $entity = $storage->create($values);
    $entity->save();

    return $entity;
  }

  /**
   * Gets the field values to load or create a settings entity.
   *
   * @param string $bundle
   *   The bundle.
   * @param string $entityTypeId
   *   The entity type ID.
   * @param array $context
   *   Additional context.
   *
   * @return array
   *   The values.
   */
  protected function getValues(string $bundle, string $entityTypeId = 'settings', array $context = []): array {
    $definition = $this->entityTypeManager
      ->getDefinition($entityTypeId);

    $values = [
      $definition->getKey('bundle') => $bundle,
    ];

    if (isset($context['langcode'])) {
      $values['langcode'] = $context['langcode'];
    }

    $context['entityTypeId'] = $entityTypeId;
    $context['bundle'] = $bundle;

    $this->moduleHandler->alter('eck_site_setting_values', $values, $context);

    return $values;
  }

}
