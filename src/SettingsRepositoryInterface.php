<?php

namespace Drupal\eck_site_settings;

use Drupal\eck\EckEntityInterface;
use Drupal\eck\EckEntityTypeInterface;

/**
 * A service for loading settings related entity types, bundles and entities.
 */
interface SettingsRepositoryInterface {

  /**
   * Get all settings entity types.
   *
   * @return \Drupal\eck\EckEntityTypeInterface[]
   *   An array of ECK entity types.
   */
  public function getEntityTypes(): array;

  /**
   * Get all settings bundles for a given entity type.
   *
   * @param \Drupal\eck\EckEntityTypeInterface $entityType
   *   The ECK entity type.
   *
   * @return \Drupal\eck\EckEntityBundleInterface[]
   *   An array of ECK entity bundles.
   */
  public function getBundles(EckEntityTypeInterface $entityType): array;

  /**
   * Check if a given entity type is a site settings entity type.
   *
   * @param string $entityTypeId
   *   The entity type ID.
   *
   * @return bool
   *   TRUE if the bundle exists, FALSE otherwise.
   */
  public function isSetting(string $entityTypeId): bool;

  /**
   * Get a setting entity by bundle and entity type ID.
   *
   * @param string $bundle
   *   The bundle name.
   * @param string $entityTypeId
   *   The entity type ID.
   * @param array $context
   *   Additional context to pass to the entity load method.
   *
   * @return \Drupal\eck\EckEntityInterface
   *   The setting entity.
   */
  public function getSetting(string $bundle, string $entityTypeId = 'settings', array $context = []): EckEntityInterface;

  /**
   * Get a setting entity by class name.
   *
   * Can be useful when using custom entity type/bundle classes.
   *
   * @param class-string<T> $className
   *   The entity class name.
   * @param array $context
   *   Additional context to pass to the entity load method.
   *
   * @return T|null
   *   The setting entity or NULL if not found.
   *
   * @template T of \Drupal\eck\EckEntityInterface
   */
  public function getSettingByClass(string $className, array $context = []): ?EckEntityInterface;

}
