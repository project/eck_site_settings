<?php

namespace Drupal\eck_site_settings\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Provides a controller for redirecting to a specific site setting.
 */
class SettingRedirectController implements ContainerInjectionInterface {

  /**
   * The settings repository.
   *
   * @var \Drupal\eck_site_settings\SettingsRepositoryInterface
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->settings = $container->get('eck_site_settings.settings_repository');

    return $instance;
  }

  /**
   * Returns a redirect response to the edit form of a specific site setting.
   */
  public function __invoke(string $bundle, ?string $entityType): RedirectResponse {
    try {
      if ($entityType === NULL) {
        $setting = $this->settings->getSetting($bundle);
      }
      else {
        $setting = $this->settings->getSetting($bundle, $entityType);
      }
    }
    catch (PluginNotFoundException $exception) {
      throw new NotFoundHttpException('This setting entity type does not exist', $exception);
    }
    catch (\InvalidArgumentException $exception) {
      throw new NotFoundHttpException('This setting bundle does not exist', $exception);
    }

    return new RedirectResponse($setting->toUrl('edit-form')->toString());
  }

}
