<?php

namespace Drupal\eck_site_settings\Controller;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a controller for the site settings overview.
 */
class SettingOverviewController implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The settings repository.
   *
   * @var \Drupal\eck_site_settings\SettingsRepositoryInterface
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = new static();
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->settings = $container->get('eck_site_settings.settings_repository');

    return $instance;
  }

  /**
   * Returns the site settings overview.
   */
  public function __invoke(): array {
    $entityTypes = $this->settings->getEntityTypes();
    $rows = [];

    foreach ($entityTypes as $entityType) {
      if (count($entityTypes) > 1) {
        $rows[] = [
          'label' => Markup::create(sprintf('<strong>%s</strong>', $entityType->label())),
          'description' => $entityType->get('description'),
          'operations' => [],
        ];
      }

      $bundles = $this->settings->getBundles($entityType);
      foreach ($bundles as $bundle) {
        $setting = $this->settings->getSetting($bundle->id(), $entityType->id());
        $operations = $this->entityTypeManager->getListBuilder($entityType->id())->getOperations($setting);

        $rows[] = [
          'label' => $bundle->label(),
          'description' => $bundle->get('description'),
          'operations' => [
            'data' => [
              '#type' => 'operations',
              '#links' => $operations,
            ],
          ],
        ];
      }
    }

    return [
      '#theme' => 'table',
      '#rows' => $rows,
      '#empty' => $this->t('No settings found'),
      '#header' => [
        $this->t('Label'),
        $this->t('Description'),
        $this->t('Operations'),
      ],
    ];
  }

}
