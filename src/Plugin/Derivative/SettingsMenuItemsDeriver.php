<?php

namespace Drupal\eck_site_settings\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\eck\Entity\EckEntityBundle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides menu links for the different settings entity types & bundles.
 */
class SettingsMenuItemsDeriver extends DeriverBase implements ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The settings repository.
   *
   * @var \Drupal\eck_site_settings\SettingsRepositoryInterface
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $basePluginId) {
    $instance = new static();
    $instance->settings = $container->get('eck_site_settings.settings_repository');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $entityTypes = $this->settings->getEntityTypes();

    foreach ($entityTypes as $entityType) {
      if (count($entityTypes) > 1) {
        $this->derivatives[sprintf('eck_site_settings.settings.%s', $entityType->id())] = [
          'title' => $entityType->label(),
          'route_name' => '<nolink>',
          'route_parameters' => [],
        ] + $base_plugin_definition;
        $bundleParent = sprintf('eck_site_settings.settings.types:eck_site_settings.settings.%s', $entityType->id());
      }
      else {
        $bundleParent = $base_plugin_definition['parent'];
      }

      $bundles = $this->settings->getBundles($entityType);
      foreach ($bundles as $bundle) {
        assert($bundle instanceof EckEntityBundle);
        $editUrl = Url::fromRoute('eck_site_settings.setting', [
          'entityType' => count($entityTypes) > 1 ? $entityType->id() : NULL,
          'bundle' => $bundle->id(),
        ]);

        $this->derivatives[sprintf('eck_site_settings.settings.%s.%s', $entityType->id(), $bundle->id())] = [
          'title' => $bundle->label(),
          'description' => $bundle->description,
          'route_name' => $editUrl->getRouteName(),
          'route_parameters' => $editUrl->getRouteParameters(),
          'parent' => $bundleParent,
        ] + $base_plugin_definition;
      }
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
