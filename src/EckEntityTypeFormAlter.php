<?php

namespace Drupal\eck_site_settings;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\eck\EckEntityTypeInterface;

/**
 * Alters the ECK entity type form.
 */
class EckEntityTypeFormAlter {

  use StringTranslationTrait;

  /**
   * The settings repository.
   *
   * @var \Drupal\eck_site_settings\SettingsRepositoryInterface
   */
  protected $settings;

  /**
   * Constructs a new EckEntityTypeFormEventSubscriber object.
   *
   * @param \Drupal\eck_site_settings\SettingsRepositoryInterface $settings
   *   The settings repository.
   */
  public function __construct(
    SettingsRepositoryInterface $settings
  ) {
    $this->settings = $settings;
  }

  /**
   * Add a checkbox to ECK entity type forms to mark them as settings.
   */
  public function alterForm(array &$form, FormStateInterface $form_state): void {
    $type = $form_state->getFormObject()->getEntity();
    assert($type instanceof EckEntityTypeInterface);

    $form['settings']['standalone_url']['#states'] = [
      'disabled' => [
        ':input[name="eck_site_settings"]' => ['checked' => TRUE],
      ],
    ];

    $form['settings']['eck_site_settings'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use this entity type for site settings'),
      '#default_value' => $type->getThirdPartySetting('eck_site_settings', 'enabled', FALSE),
      '#description' => $this->t('A single entity will be created after you save this entity type.'),
    ];

    $form['#entity_builders'][] = [static::class, 'formBuilder'];
  }

  /**
   * Store the setting in the entity type's third party settings.
   */
  public static function formBuilder($entity_type, EckEntityTypeInterface $type, &$form, FormStateInterface $form_state): void {
    $enabled = $form_state->getValue('eck_site_settings');
    $type->setThirdPartySetting('eck_site_settings', 'enabled', $enabled);

    if ($enabled) {
      $type->set('standalone_url', FALSE);
    }
  }

}
