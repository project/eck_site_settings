<?php

namespace Drupal\eck_site_settings;

/**
 * Migrate site settings from similar modules to eck_site_settings.
 */
interface ModuleMigrationInterface {

  /**
   * Migrate fields & data from site_settings.
   *
   * @throws \RuntimeException
   *   If the site_settings module is not enabled.
   */
  public function fromSiteSettings(): void;

  /**
   * Migrate fields & data from wmsettings.
   *
   * @throws \RuntimeException
   *   If the site_settings module is not enabled.
   */
  public function fromWmSettings(): void;

}
