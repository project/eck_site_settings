<?php

namespace Drupal\eck_site_settings;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayBase;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\eck\EckEntityBundleInterface;
use Drupal\eck\EckEntityInterface;
use Drupal\site_settings\SiteSettingEntityInterface;

/**
 * Migrate site settings from similar modules to eck_site_settings.
 */
class ModuleMigration implements ModuleMigrationInterface {

  /**
   * ModuleMigration constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected ModuleHandlerInterface $moduleHandler,
    protected ConfigFactoryInterface $configFactory,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function fromSiteSettings(): void {
    if (!$this->moduleHandler->moduleExists('site_settings')) {
      throw new \RuntimeException('Can only migrate settings if the site_settings module is enabled.');
    }

    /** @var \Drupal\site_settings\SiteSettingEntityTypeInterface[] $oldBundles */
    $oldBundles = $this->entityTypeManager
      ->getStorage('site_setting_entity_type')
      ->loadMultiple();

    foreach ($oldBundles as $oldBundle) {
      // Migrate bundle.
      $bundleStorage = $this->entityTypeManager
        ->getStorage('settings_type');

      if (!$bundle = $bundleStorage->load($oldBundle->id())) {
        $bundle = $bundleStorage->create([
          'type' => $oldBundle->id(),
          'name' => $oldBundle->label(),
          'description' => '',
        ]);
        $bundle->save();
      }

      // Migrate fields.
      $oldFields = $this->entityFieldManager
        ->getFieldDefinitions('site_setting_entity', $oldBundle->id());

      foreach ($oldFields as $oldField) {
        if (!$oldField instanceof FieldConfigInterface) {
          continue;
        }

        $oldFieldStorage = $oldField->getFieldStorageDefinition();
        $fieldStorageStorage = $this->entityTypeManager
          ->getStorage('field_storage_config');

        /** @var \Drupal\field\FieldStorageConfigInterface $fieldStorage */
        if (!$fieldStorage = $fieldStorageStorage->load(sprintf('settings.%s', $oldFieldStorage->getName()))) {
          $values = [
            'field_name' => $oldFieldStorage->getName(),
            'entity_type' => 'settings',
            'type' => $oldFieldStorage->getType(),
            'cardinality' => $oldFieldStorage->getCardinality(),
            'translatable' => $oldFieldStorage->isTranslatable(),
            'settings' => $oldFieldStorage->getSettings(),
          ];
          $fieldStorage = $fieldStorageStorage->create($values);
          $fieldStorage->save();
        }

        $fieldConfigStorage = $this->entityTypeManager
          ->getStorage('field_config');

        /** @var \Drupal\Core\Field\FieldConfigInterface $field */
        if (!$field = $fieldConfigStorage->load(sprintf('settings.%s.%s', $bundle->id(), $oldFieldStorage->getName()))) {
          $values = [
            'field_name' => $oldField->getName(),
            'entity_type' => 'settings',
            'bundle' => $bundle->id(),
            'translatable' => $oldField->isTranslatable(),
            'required' => $oldField->isRequired(),
            'field_type' => $oldField->getType(),
            'description' => $oldField->getDescription(),
            'label' => $oldField->getLabel(),
            'settings' => $oldField->getSettings(),
            'third_party_settings' => [],
          ];

          foreach ($oldField->getThirdPartyProviders() as $module) {
            $values['third_party_settings'][$module] = $oldField->getThirdPartySettings($module);
          }

          $field = $fieldConfigStorage->create($values);
          $field->save();
        }
      }

      $this->entityFieldManager->clearCachedFieldDefinitions();

      // Migrate displays.
      foreach (['entity_form_display', 'entity_view_display'] as $displayType) {
        $sourceDisplays = $this->getDisplays($displayType, 'site_setting_entity', $bundle->id());

        foreach ($sourceDisplays as $sourceDisplay) {
          $this->cloneDisplay($sourceDisplay, 'settings', $bundle->id());
        }
      }

      // Migrate data.
      $fieldsToIgnore = array_diff_key(
        $this->entityFieldManager->getFieldDefinitions('site_setting_entity', $oldBundle->id()),
        $this->entityFieldManager->getFieldDefinitions('settings', $oldBundle->id()),
      );
      $fieldsToIgnore = array_keys($fieldsToIgnore);
      $fieldsToIgnore[] = 'id';
      $fieldsToIgnore[] = 'uuid';

      $oldEntity = $this->getSiteSetting($oldBundle->id());
      if (!$oldEntity instanceof SiteSettingEntityInterface) {
        continue;
      }

      $values = array_diff_key(
        $oldEntity->toArray(),
        array_flip($fieldsToIgnore),
      );

      if (!$entity = $this->getEckSiteSetting($bundle->id())) {
        $entity = $this->entityTypeManager
          ->getStorage('settings')
          ->create($values);
        $entity->save();
      }
      assert($entity instanceof EckEntityInterface);

      foreach ($values as $fieldName => $fieldValue) {
        $entity->set($fieldName, $fieldValue);
      }

      $entity->save();

      foreach ($oldEntity->getTranslationLanguages(FALSE) as $language) {
        $oldTranslation = $oldEntity->getTranslation($language->getId());

        if ($entity->hasTranslation($language->getId())) {
          $translation = $entity->getTranslation($language->getId());
        }
        else {
          $translation = $entity->addTranslation($language->getId());
        }

        $values = array_diff_key(
          $oldTranslation->toArray(),
          array_flip($fieldsToIgnore),
        );

        foreach ($values as $fieldName => $fieldValue) {
          $translation->set($fieldName, $fieldValue);
        }

        $translation->save();
      }
    }

    // Migrate permissions.
    $roles = $this->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple();

    $permissionMap = [
      'add site setting entities' => NULL,
      'access site settings overview' => 'access site settings overview',
      'delete site setting entities' => NULL,
      'edit site setting entities' => 'edit any settings entities',
      'view published site setting entities' => NULL,
      'view unpublished site setting entities' => NULL,
    ];

    foreach ($roles as $role) {
      foreach ($permissionMap as $oldPermission => $newPermission) {
        if (!$role->hasPermission($oldPermission)) {
          continue;
        }

        $role->revokePermission($oldPermission);
        $role->save();

        if ($newPermission !== NULL) {
          $role->grantPermission($newPermission);
          $role->save();
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function fromWmSettings(): void {
    if (!$this->moduleHandler->moduleExists('wmsettings')) {
      throw new \RuntimeException('Can only migrate settings if the wmsettings module is enabled.');
    }

    // Enable eck_site_settings on the existing settings entity type.
    $entityTypeStorage = $this->entityTypeManager
      ->getStorage('eck_entity_type');

    $entityType = $entityTypeStorage->load('settings');
    $entityType->setThirdPartySetting('eck_site_settings', 'enabled', TRUE);
    $entityType->save();

    // Remove wmsettings dependency from the settings entity type.
    $config = $this->configFactory->getEditable('eck.eck_entity_type.settings');
    $modules = $config->get('dependencies.enforced.module') ?? [];

    if (in_array('wmsettings', $modules)) {
      $modules = array_diff($modules, ['wmsettings']);
      $config->set('dependencies.enforced.module', $modules);
      $config->save();
    }

    // Move labels/descriptions from config to the bundle entities.
    $settings = $this->configFactory->getEditable('wmsettings.settings');
    $bundleStorage = $this->entityTypeManager
      ->getStorage('settings_type');

    foreach ($settings->get('keys') ?? [] as $keyData) {
      $bundleEntity = $bundleStorage->load($keyData['bundle']);
      assert($bundleEntity instanceof EckEntityBundleInterface);

      $bundleEntity->set('name', $keyData['label']);
      $bundleEntity->set('description', $keyData['desc']);
      $bundleEntity->save();
    }

    $settings->delete();

    // Migrate permissions.
    $roles = $this->entityTypeManager
      ->getStorage('user_role')
      ->loadMultiple();

    $permissionMap = [
      'administer wmsettings content' => 'access site settings overview',
      'delete wmsettings group' => NULL,
    ];

    foreach ($roles as $role) {
      foreach ($permissionMap as $oldPermission => $newPermission) {
        if (!$role->hasPermission($oldPermission)) {
          continue;
        }

        $role->revokePermission($oldPermission);
        $role->save();

        if ($newPermission !== NULL) {
          $role->grantPermission($newPermission);
          $role->save();
        }
      }
    }
  }

  /**
   * Load a site_settings entity.
   *
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\site_settings\SiteSettingEntityInterface|null
   *   The site_settings entity or NULL if it does not exist.
   */
  protected function getSiteSetting(string $bundle): ?SiteSettingEntityInterface {
    $definition = $this->entityTypeManager
      ->getDefinition('site_setting_entity');
    $storage = $this->entityTypeManager
      ->getStorage('site_setting_entity');
    $ids = $storage->getQuery()
      ->condition($definition->getKey('bundle'), $bundle)
      ->range(0, 1)
      ->execute();

    if ($ids === []) {
      return NULL;
    }

    return $storage->load(reset($ids));
  }

  /**
   * Load an ECK settings entity.
   *
   * @param string $bundle
   *   The bundle.
   *
   * @return \Drupal\eck\EckEntityInterface|null
   *   The ECK settings entity or NULL if it does not exist.
   */
  protected function getEckSiteSetting(string $bundle): ?EckEntityInterface {
    $definition = $this->entityTypeManager
      ->getDefinition('settings');
    $storage = $this->entityTypeManager
      ->getStorage('settings');
    $ids = $storage->getQuery()
      ->condition($definition->getKey('bundle'), $bundle)
      ->range(0, 1)
      ->execute();

    if ($ids === []) {
      return NULL;
    }

    return $storage->load(reset($ids));
  }

  /**
   * Get all displays for a given entity type and bundle.
   *
   * @return \Drupal\Core\Entity\Display\EntityDisplayInterface[]
   *   The displays.
   */
  protected function getDisplays(string $type, string $entityTypeId, string $bundle): array {
    $storage = $this->entityTypeManager
      ->getStorage($type);
    $ids = $storage->getQuery()
      ->condition('targetEntityType', $entityTypeId)
      ->condition('bundle', $bundle)
      ->range(0, 1)
      ->execute();

    return $storage->loadMultiple($ids);
  }

  /**
   * Clone or merge a form or view display to a new bundle.
   *
   * Display settings are copied to the new bundle.
   *  - Fields which are present on the source but not the target are ignored.
   * If the target already has this display:
   *  - Fields which are on the both source and target have their settings
   *    overwritten.
   *  - Fields which are on the target only but not the source have their
   *    settings left untouched.
   *
   * @param \Drupal\Core\Entity\EntityDisplayBase $sourceDisplay
   *   The entity display (form or view) to clone.
   * @param string $destinationEntityTypeId
   *   The destination entity type ID.
   * @param string $destinationBundle
   *   The destination bundle.
   *
   * @see \Drupal\field_tools\DisplayCloner
   */
  protected function cloneDisplay(EntityDisplayBase $sourceDisplay, string $destinationEntityTypeId, string $destinationBundle) {
    $displayEntityType = $sourceDisplay->getEntityTypeId();

    // Have to deduce the context from the entity type, as there's no accessor
    // for $displayContext: see https://www.drupal.org/node/2823807.
    if ($displayEntityType == 'entity_form_display') {
      $context = 'form';
    }
    else {
      $context = 'view';
    }

    $sourceEntityTypeId = $sourceDisplay->getTargetEntityTypeId();
    $sourceBundle = $sourceDisplay->getTargetBundle();
    $modeName = $sourceDisplay->getMode();

    // Try to load the destination display.
    $displayStorage = $this->entityTypeManager
      ->getStorage($displayEntityType);
    $destinationDisplay = $displayStorage->load($destinationEntityTypeId . '.' . $destinationBundle . '.' . $modeName);

    if (empty($destinationDisplay)) {
      $values = $sourceDisplay->toArray();
      unset($values['uuid']);
      $values['targetEntityType'] = $destinationEntityTypeId;
      $values['bundle'] = $destinationBundle;
      $destinationDisplay = $displayStorage->create($values);
    }

    // Get all fields on source & destination bundles.
    $sourceBundleFields = array_filter($this->entityFieldManager->getFieldDefinitions($sourceEntityTypeId, $sourceBundle), function ($field_definition) use ($context) {
      return $field_definition->isDisplayConfigurable($context);
    });
    $destinationBundleFields = array_filter($this->entityFieldManager->getFieldDefinitions($destinationEntityTypeId, $destinationBundle), function ($field_definition) use ($context) {
      return $field_definition->isDisplayConfigurable($context);
    });

    // Get the visible display components from the source display
    // and copy them to the destination.
    $components = $sourceDisplay->getComponents();
    foreach ($components as $fieldName => $component) {
      // Skip fields that do not exist on the destination bundle.
      if (!isset($destinationBundleFields[$fieldName])) {
        continue;
      }

      $destinationDisplay->setComponent($fieldName, $component);
    }

    // Get the hidden display components from the source display
    // and copy them to the destination.
    $hiddenComponents = array_diff_key(
          $sourceBundleFields,
          $sourceDisplay->getComponents()
      );
    foreach ($hiddenComponents as $fieldName => $component) {
      // Skip fields that do not exist on the destination bundle.
      if (!isset($destinationBundleFields[$fieldName])) {
        continue;
      }

      $destinationDisplay->removeComponent($fieldName);
    }

    // Copy field groups.
    if ($this->moduleHandler->moduleExists('field_group')) {
      $fieldGroupSettings = $sourceDisplay->getThirdPartySettings('field_group');
      foreach ($fieldGroupSettings as $groupId => $groupSettings) {
        // Remove any fields in groups which do not exist on the destination
        // bundle.
        foreach ($groupSettings['children'] as $index => $child_field) {
          if (isset($destinationBundleFields[$child_field])) {
            continue;
          }
          if (isset($fieldGroupSettings[$child_field])) {
            continue;
          }
          unset($groupSettings['children'][$index]);
        }

        // Skip groups which don't have any fields left in them.
        if (empty($groupSettings['children'])) {
          continue;
        }

        // @todo rekey the numeric 'children' array?
        // Set the group in the destination display.
        // (There's no setThirdPartySetting*s*() method...)
        $destinationDisplay->setThirdPartySetting('field_group', $groupId, $groupSettings);
      }
    }

    // Save the display.
    $destinationDisplay->save();
  }

}
