# ECK Site Settings


## Introduction

This module enables the use of Entity Construction Kit (ECK) entities as global,
site-wide setting entities. It's an alternative for modules like [Site Settings
and Labels](https://www.drupal.org/project/site_settings) or 
[Config Pages](https://www.drupal.org/project/config_pages).


## Requirements

This module requires the [Entity Construction Kit (ECK) module](https://www.drupal.org/project/eck).

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

During the module installation, a Settings ECK entity type is automatically
created, together with a General bundle. This allows you to immediately start a
general config page by adding fields to the General bundle.

### Bundles

If the amount of fields on the General bundle starts to grow too much, or if you
want to start grouping fields into different pages, you can create more bundles.
All settings bundles are listed on the site settings page
(/admin/content/site-settings).


### Entity types

If you want to go even further and start grouping bundles, you can create more
ECK entity types and mark them as site settings entity types. You can do this
by checking the Use this entity type for site settings box on the entity type
create page. As long as you have more than one settings entity type, the
bundles will be grouped by their entity type on the overview page and in the
admin menu.


### Permissions

This module defines one permission, Access site settings overview, which is
required to access the overview page. Edit access to individual entity types is
controlled by the permissions defined by ECK. Create, view, and delete
permissions are hidden from the permissions page since they are irrelevant for
settings entities. If you want to control access by bundle instead of by entity
type, you can try the [ECK Bundle Permissions module](https://www.drupal.org/project/eck_bundle_permissions/).

### Twig

The module provides a Twig extension to load settings entities in any Twig
template. The function takes two arguments: the bundle name and the entity type
ID. The last one is optional and defaults to `settings`, the entity type which 
is created by default.

Once you have the entity there are a couple ways you can extract information
from it. Note that the following recommendations are not specific to this
module, they apply to any entity object in a Twig context.

If you want to render the field as configured in the entity view display, you
can use the view Twig filter which is provided by the 
[Twig Tweak module](https://www.drupal.org/project/twig_tweak):
```
{{ site_settings('general').field_some_rich_text|view }}
```
If you want to get the raw value from the field, you can access it using the
field name + property name combination:
```
{{ site_settings('general').field_some_rich_text.processed }}
```
If you want to get a file or image URL from a file or image field, you can use
the [file_url](https://git.drupalcode.org/project/twig_tweak/-/blob/3.x/docs/cheat-sheet.md#file-url) or [image_style](https://git.drupalcode.org/project/twig_tweak/-/blob/3.x/docs/cheat-sheet.md#image-style) filters provided by the [Twig Tweak module](https://www.drupal.org/project/twig_tweak).
```
{{ site_settings('general').field_image|file_uri }}
{{ site_settings('general').field_image[0]|file_uri }}

{% set image_uri = site_settings('general').field_media|file_uri %}
{% if image_uri is not null %}
  {{ image_uri|image_style('thumbnail') }}
{% endif %}
```
If you want to get the raw values without having to remember field/property
names, you can use a combination of the [Entity Model](https://www.drupal.org/project/entity_model) and 
[Entity Bundle
Scaffold](https://www.drupal.org/project/entity_bundle_scaffold) modules to 
create per-bundle subclasses with getters for your bundle fields:
```
{{ site_settings('general').getSomeRichText }}
```
or:
```
{{ site_settings('general').someRichText }}
```
or in case of an entity reference field:
```
{{ site_settings('general').someReferencedEntity.someOtherField }}
```


## Migration from other modules
### Site Settings and Labels
Add the following code to a [deploy hook](https://www.drush.org/latest/deploycommand/), or find another way to run it during deployment, after config has been imported:
```
<?php

/**
 * @file
 * Contains deploy hooks.
 */

/**
 * Migrate site_settings to eck_site_settings.
 */
function my_module_deploy_8001(): void {
  \Drupal::getContainer()
    ->get('eck_site_settings.module_migration')
    ->fromSiteSettings();
}

```
All bundles, fields, form/view displays and entity data will be copied automatically. Before deploying, run the code locally and export + commit any config changes.


## Maintainers

Current maintainers:
- Dieter Holvoet - [dieterholvoet](https://www.drupal.org/u/dieterholvoet)
