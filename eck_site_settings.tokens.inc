<?php

/**
 * @file
 * Tokens for the ECK Site Settings module.
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements @see hook_token_info().
 */
function eck_site_settings_token_info(): array {
  $types['eck_site_settings'] = [
    'name' => t('ECK Site Settings'),
    'description' => t('Tokens related to the ECK site settings.'),
  ];

  // Get all site settings.
  $settings = \Drupal::getContainer()
    ->get('eck_site_settings.settings_repository');

  $tokens = [];
  foreach ($settings->getEntityTypes() as $entityType) {
    foreach ($settings->getBundles($entityType) as $bundle) {
      $tokens['eck_site_settings'][sprintf('%s-%s', $entityType->id(), $bundle->id())] = [
        'name' => sprintf('%s (%s)', $entityType->label(), $bundle->label()),
        'description' => t('Field values from the site settings entity.'),
        'type' => 'settings',
      ];
    }
  }

  return [
    'types' => $types,
    'tokens' => $tokens,
  ];
}

/**
 * Implements @see hook_tokens().
 */
function eck_site_settings_tokens(string $type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];

  if ($type !== 'eck_site_settings') {
    return $replacements;
  }

  $settings = \Drupal::getContainer()
    ->get('eck_site_settings.settings_repository');

  foreach ($tokens as $name => $original) {
    $parts = explode(':', $name);
    $siteSettingName = array_shift($parts);
    [$entityTypeId, $bundle] = explode('-', $siteSettingName);

    try {
      $siteSettingEntity = $settings->getSetting($bundle, $entityTypeId);
    }
    catch (\InvalidArgumentException $exception) {
      continue;
    }

    if ($entityTokens = \Drupal::token()->findWithPrefix($tokens, $siteSettingName)) {
      $replacements += \Drupal::token()->generate('settings', $entityTokens, ['settings' => $siteSettingEntity], $options, $bubbleable_metadata);
    }
  }

  return $replacements;
}
