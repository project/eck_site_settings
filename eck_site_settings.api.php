<?php

/**
 * @file
 * Documents hooks provided by the eck_site_settings module.
 */

/**
 * Alter the context used for loading or creating settings entities.
 *
 * @param array{entityTypeId: string, bundle: string, langcode: string} $context
 *   The additional context. Values must be scalar or stringable.
 */
function hook_eck_site_setting_context_alter(array &$context): void {
  // Always load English settings, regardless of the current language.
  $context['langcode'] = 'en';
}

/**
 * Alter the values used for loading or creating settings entities.
 *
 * @param array $values
 *   The values of the settings entity.
 * @param array{entityTypeId: string, bundle: string, langcode: string} $context
 *   Any additional context.
 */
function hook_eck_site_setting_values_alter(array &$values, array $context): void {
  // Change the title of the settings entity.
  $values['title'] = 'My custom title';

  // Add a new field to the settings entity.
  $values['field_custom'] = 'My custom value';
}
